<?php

namespace CodeEducation\Services;

use CodeEducation\Repositories\ProjectNoteRepository;
use CodeEducation\Validators\ProjectNoteValidator;
use Prettus\Validator\Exceptions\ValidatorException;

class ProjectNoteService
{
    /**
     * @var ProjectNoteRepository
     */
    protected $repository;
    /**
     * @var ProjectValidator
     */
    protected $projectValidator;

    public function __construct(ProjectNoteRepository $repository, ProjectNoteValidator $projectValidator)
    {
        $this->repository = $repository;
        $this->projectValidator = $projectValidator;
    }

    public function create(array $data)
    {
        try{
            $this->projectValidator->with($data)->passesOrFail();
            return $this->repository->create($data);
        }catch (ValidatorException $e)
        {
            return [
              'erro' => true,
              'message' => $e->getMessageBag()
            ];
        }
    }
    public function update(array $data, $id)
    {
        try{
            $this->projectValidator->with($data)->passesOrFail();
            return $this->repository->update($data, $id);
        }catch (ValidatorException $e)
        {
            return [
              'erro' => true,
              'message' => $e->getMessageBag()
            ];
        }
    }
}