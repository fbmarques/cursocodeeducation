<?php

namespace CodeEducation\Services;

use CodeEducation\Repositories\ProjectRepository;
use CodeEducation\Validators\ProjectValidator;
use Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Contracts\Filesystem\Factory as Storage;

class ProjectService
{
    /**
     * @var ProjectRepository
     */
    protected $repository;
    /**
     * @var ProjectValidator
     */
    protected $projectValidator;
    /**
     * @var Filesystem
     */
    private $filesystem;
    /**
     * @var Factory
     */
    private $factory;

    public function __construct(ProjectRepository $repository, ProjectValidator $projectValidator,
                                Filesystem $filesystem, Storage $storage)
    {
        $this->repository = $repository;
        $this->projectValidator = $projectValidator;
        $this->filesystem = $filesystem;
        $this->storage = $storage;
    }

    public function create(array $data)
    {
        try{
            $this->projectValidator->with($data)->passesOrFail();
            return $this->repository->create($data);
        }catch (ValidatorException $e)
        {
            return [
              'erro' => true,
              'message' => $e->getMessageBag()
            ];
        }
    }
    public function update(array $data, $id)
    {
        try{
            $this->projectValidator->with($data)->passesOrFail();
            return $this->repository->update($data, $id);
        }catch (ValidatorException $e)
        {
            return [
              'erro' => true,
              'message' => $e->getMessageBag()
            ];
        }
    }

    public function createFile(array $data)
    {
        #$projectId, $exetension, $name, $description

        $project = $this->repository->skipPresenter()->find($data['project_id']);
        $projectFile = $project->files()->create($data);

        $this->storage->put($projectFile->id.".".$data['extension'], $this->filesystem->get($data['file']));



    }
}