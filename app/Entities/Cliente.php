<?php

namespace CodeEducation\Entities;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $fillable = ['nome', 'responsavel', 'endreco','cidade','uf','tel','cel'];

}
