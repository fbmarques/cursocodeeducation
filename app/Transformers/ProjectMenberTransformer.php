<?php

namespace CodeEducation\Transformers;
use CodeEducation\Entities\User;
use League\Fractal\TransformerAbstract;

class ProjectMenberTransformer extends TransformerAbstract
{
    public function transform(User $member)
    {
        return [
            'member_id'  => $member->id,
            'name'       => $member->name
        ];
    }
}