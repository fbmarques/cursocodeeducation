<?php

namespace CodeEducation\Validators;

use Prettus\Validator\LaravelValidator;

class ClienteValidator extends LaravelValidator
{
    protected $rules = [
        'nome' => 'required|max:255|min:10'
    ];

}