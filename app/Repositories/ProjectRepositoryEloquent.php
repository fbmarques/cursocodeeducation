<?php

namespace CodeEducation\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use CodeEducation\Entities\Project;
use CodeEducation\Presenters\ProjectPresenter;

/**
 * Class ProjectRepositoryEloquent
 * @package namespace CodeEducation\Repositories;
 */
class ProjectRepositoryEloquent extends BaseRepository implements ProjectRepository
{

    public function model()
    {
        return Project::class;
    }

    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * Função para verficicar se o usuário é dono do projeto e pode visualizar informações
     */
    public function isOwner($projectId, $userId)
    {

        if(count($this->findWhere(['id' => $projectId, 'owner_id' => $userId])))
        {
            return true;
        }

        return false;
    }

    public function hasMember($projectId, $memberId)
    {
        $project = $this->find($projectId);

        foreach($project->members as $member)
        {
            if($member->id == $memberId)
            {
                return true;
            }

            return false;
        }

    }

    public function presenter()
    {
        return ProjectPresenter::class;
    }
}
