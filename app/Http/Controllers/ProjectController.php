<?php

namespace CodeEducation\Http\Controllers;

use CodeEducation\Repositories\ProjectRepository;
use CodeEducation\Services\ProjectService;
use Illuminate\Http\Request;


class ProjectController extends Controller
{
    /**
     * @var ProjectRepository
     */
    protected $repository;
    /**
     * @var ProjectService
     */
    protected $service;

    public function __construct(ProjectRepository $repository, ProjectService $service)
    {
        $this->repository = $repository;
        $this->service = $service;
    }

    public function index()
    {
       return $this->repository->findWhere(['owner_id' => \Authorizer::getResourceOwnerId()]);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        return $this->service->create($request->all());
    }

    public function show($id)
    {
        #verificando se o projeto é realmente desse usuário que está logado
        if($this->checkProjectPermissions($id) == false)
        {
            return ['erro' => 'Acesso negado!'];
        }

        return $this->repository->find($id);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        #verificando se o projeto é realmente desse usuário que está logado
        if($this->checkProjetOwner($id) == false)
        {
            return ['erro' => 'Acesso negado!'];
        }

        return $this->service->update($request->all(),$id);
    }

    public function destroy($id)
    {
        #verificando se o projeto é realmente desse usuário que está logado
        if($this->checkProjetOwner($id) == false)
        {
            return ['erro' => 'Acesso negado!'];
        }

        $this->repository->delete($id);
    }

    private function checkProjcetOwner($projectId)
    {
        $userId = \Authorizer::getResourceOwnerId();
        return $this->repository->isOwner($projectId, $userId);

    }
    private function checkProjetMember($projectId)
    {
        $userId = \Authorizer::getResourceOwnerId();
        return $this->repository->hasMember($projectId, $userId);

    }

    private function checkProjectPermissions($projectId)
    {
        if( $this->checkProjcetOwner($projectId) or
            $this->checkProjetMember($projectId))
        {
            return true;
        }

        return false;
    }
}
