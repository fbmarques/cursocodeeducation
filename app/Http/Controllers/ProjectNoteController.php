<?php

namespace CodeEducation\Http\Controllers;

use CodeEducation\Repositories\ProjectNoteRepository;
use CodeEducation\Services\ProjectNoteService;
use Illuminate\Http\Request;

class ProjectNoteController extends Controller
{
    /**
     * @var ProjectNoteRepository
     */
    protected $repository;
    /**
     * @var ProjecNotetService
     */
    protected $service;

    public function __construct(ProjectNoteRepository $repository, ProjectNoteService $service)
    {
        $this->repository = $repository;
        $this->service = $service;
    }

    public function index($id)
    {
       return $this->repository->findWhere(['project_id' => $id]);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        return $this->service->create($request->all());
        #return $this->repository->create($request->all());
        #return Cliente::create($request->all());
    }

    public function show($id, $noteId)
    {
        #return Cliente::find($id);
        return $this->repository->findWhere(['project_id' => $id, 'id' => $noteId]);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id, $noteId)
    {
        #Cliente::find($id)->delete();
        $this->repository->delete($noteId);
    }
}
