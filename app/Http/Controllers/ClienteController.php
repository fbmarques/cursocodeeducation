<?php

namespace CodeEducation\Http\Controllers;

use CodeEducation\Repositories\ClienteRepository;
use CodeEducation\Services\ClienteService;
use Illuminate\Http\Request;

class ClienteController extends Controller
{
    /**
     * @var ClienteRepository
     */
    protected $repository;
    /**
     * @var ClienteService
     */
    protected $validator;

    public function __construct(ClienteRepository $repository, ClienteService $service)
    {
        $this->repository = $repository;
        $this->service = $service;
    }

    public function index()
    {
       return $this->repository->all();
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        return $this->service->create($request->all());
        #return $this->repository->create($request->all());
        #return Cliente::create($request->all());
    }

    public function show($id)
    {
        #return Cliente::find($id);
        return $this->repository->find($id);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        #Cliente::find($id)->delete();
        $this->repository->delete($id);
    }
}
