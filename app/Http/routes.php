<?php

Route::post('oauth/access_token', function() {
    return Response::json(Authorizer::issueAccessToken());
});

Route::group(['middleware' => 'oauth'], function(){

    Route::resource('cliente', 'ClienteController', ['except'=>['create','edit']]);

    #Route::group(['middleware' => 'CheckProjectOwner'], function(){

        Route::resource('project','ProjectController', ['except'=>['create','edit']]);

    #});


    Route::group(['prefix' => 'project'], function(){

        Route::get('{id}/note', 'ProjectNoteController@index');
        Route::post('{id}/note', 'ProjectNoteController@store');
        Route::get('{id}/note{noteid}', 'ProjectNoteController@show');
        Route::delete('note/{id}', 'ProjectNoteController@index');

        Route::post('{id}/file', 'ProjectFileController@store');

    });
});

Route::get('/', function () {
    return view('welcome');
});
