<?php

namespace CodeEducation\Http\Middleware;

use Closure;
use CodeEducation\Repositories\ProjectRepository;

class CheckProjectOwner
{
    private $repository;

    public function __construct(ProjectRepository $repository)
    {
        $this->repository = $repository;
    }

    public function handle($request, Closure $next)
    {
        $userId = \Authorizer::getResourceOwnerId();

        $projectId = $request->project;

        if($this->repository->isOwner($projectId, $userId) == false){

            return ['error' => 'Access forbidden'];
        }

        return $next($request);
    }
}
