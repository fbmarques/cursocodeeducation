<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientesTable extends Migration
{

    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('responsavel');
            $table->string('endreco');
            $table->string('cidade');
            $table->string('uf');
            $table->string('tel');
            $table->string('cel');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('clientes');
    }
}
