<?php

use Illuminate\Database\Seeder;

class ProjectsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        #\CodeEducation\Entities\Project::truncate();
        factory(\CodeEducation\Entities\Project::class,100)->create();
    }
}
