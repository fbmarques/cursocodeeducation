<?php

use Illuminate\Database\Seeder;

class ProjectNotesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        #\CodeEducation\Entities\Project::truncate();
        factory(\CodeEducation\Entities\ProjectNote::class,10)->create();
    }
}
