<?php

use Illuminate\Database\Seeder;

class ClientesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        #\CodeEducation\Entities\Cliente::truncate();
        factory(\CodeEducation\Entities\Cliente::class,10)->create();
    }
}
