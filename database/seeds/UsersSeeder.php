<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        #\CodeEducation\Entities\User::truncate();
        factory(\CodeEducation\Entities\User::class)->create([
            'name' => 'Francis Bento Marques',
            'email' => 'fbmarques@gmail.com',
            'password' => bcrypt('123'),
            'remember_token' => str_random(10),
        ]);
        factory(\CodeEducation\Entities\User::class,10)->create();
    }
}
